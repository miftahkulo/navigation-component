package com.kangmifhd.navigationcomponent

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.View
import com.kangmifhd.navigationcomponent.databinding.FragmentConfirmationBinding

class ConfirmationFragment : Fragment() {

    private lateinit var binding: FragmentConfirmationBinding

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding = FragmentConfirmationBinding.bind(view)
    }
}