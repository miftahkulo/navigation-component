package com.kangmifhd.navigationcomponent

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.View
import com.kangmifhd.navigationcomponent.databinding.FragmentSpecifyAmountBinding

class SpecifyAmountFragment : Fragment() {

    private lateinit var binding: FragmentSpecifyAmountBinding

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding = FragmentSpecifyAmountBinding.bind(view)
    }
}